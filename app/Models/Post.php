<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'content',
        'is_draft',
        'is_members_only',
        'posted_at'
    ];

    protected $casts = [
        'posted_at' => 'datetime' 
    ];

    public function scopeWithAuthorPosts($query)
    {
        return $query
            ->isDraft(0)
            ->orWhere('user_id', auth()->id(), function($query) {
                $query->isDraft(1);
            });
    }

    public function scopeIsDraft($query, $status)
    {
        return $query->where('is_draft', $status);
    }

    public function scopeIsMembersOnly($query, $status)
    {
        return $query->where('is_members_only', $status);
    }
    
    public function scopeIsNotFuturePosts($query)
    {
        return $query->where('posted_at', '<=', now());
    }

    public function scopePostsForAllUsers($query)
    {
        return $query
            ->isDraft(0)
            ->isMembersOnly(0)
            ->isNotFuturePosts();
    }

    public function scopePostsForLoggedInUsers($query)
    {
        return $query
            ->withAuthorPosts()
            ->isNotFuturePosts();
    }

    public function scopeShowAllPosts($query)
    {
        return $query->with('user')
            ->when(auth()->guest(), function ($query) {
                return $query->postsForAllUsers();
            })
            ->when(auth()->check(), function ($query) {
                return $query->postsForLoggedInUsers();
            })
            ->orderByDesc('posted_at');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUppercaseTitleAttribute()
    {
        return Str::upper($this->title);
    }

    public function getPostedDateAttribute()
    {
        return $this->posted_at->format('Y-m-d');
    }

    public function getUpdatedDateAttribute()
    {
        return $this->updated_at->format('Y-m-d');
    }

    public function getInputPostedAtAttribute()
    {
        return $this->posted_at->toDateTimeLocalString();
    }

    public function setPostedAtAttribute($value)
    {
        $this->attributes['posted_at'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function getContentWithBreakAttribute()
    {
        return nl2br($this->content, false);
    }
}
