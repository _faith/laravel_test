<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Display a listing of the users' posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = auth()->user()->posts()
            ->orderByDesc('posted_at')
            ->get();

        return view('home', compact('posts'));
    }
}
