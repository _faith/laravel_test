<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->member = factory(User::class)->create();
        $this->author = factory(User::class)->create();

        $this->setupPosts();
    }

    protected function setupPosts()
    {
        \DB::table('posts')->truncate();

        // published
        factory(Post::class)->create([
            'user_id' => $this->member,
            'title' => 'Published',
            'content' => 'Published content',
            'is_draft' => 0,
            'is_members_only' => 0,
            'posted_at' => now()
        ]);

        // drafted|author
        factory(Post::class)->create([
            'user_id' => $this->author,
            'title' => 'Drafted',
            'content' => 'Drafted content',
            'is_draft' => 1,
            'is_members_only' => 0,
            'posted_at' => now()
        ]);

        // member|author
        factory(Post::class)->create([
            'user_id' => $this->author,
            'title' => 'Member',
            'content' => 'Member content',
            'is_draft' => 0,
            'is_members_only' => 1,
            'posted_at' => now()
        ]);

        // future|author
        factory(Post::class)->create([
            'user_id' => $this->author,
            'title' => 'Future',
            'content' => 'Future content',
            'is_draft' => 0,
            'is_members_only' => 0,
            'posted_at' => now()->addDay()
        ]);
    }

    /** @test */
    public function guests_can_view_posts_index()
    {
        $this->withoutExceptionHandling();

        $this->setupPosts();

        $response = $this->get(route('posts.index'));

        $response->assertStatus(200);
        $response->assertSee('PUBLISHED');
    }

    /** @test */
    public function guests_can_view_post_details_show()
    {
        $this->withoutExceptionHandling();

        $this->setupPosts();

        $response = $this->get(route('posts.show', 1));

        $response->assertStatus(200);
        $response->assertSee('Published content');
    }

    /** @test */
    public function guests_cannot_see_drafted_and_future_posts_index()
    {
        $this->withoutExceptionHandling();

        $this->setupPosts();

        $response = $this->get(route('posts.index'));

        $response->assertDontSee('MEMBER');
        $response->assertDontSee('FUTURE');
    }

    /** @test */
    public function members_can_view_members_posts_index()
    {
        $this->withoutExceptionHandling();

        $this->setupPosts();

        $response = $this
            ->actingAs($this->member)
            ->get(route('posts.index'));

        $response->assertStatus(200);
        $response->assertSee('MEMBER');
    }

    /** @test */
    public function members_cannot_see_drafted_posts_index()
    {
        $this->withoutExceptionHandling();

        $this->setupPosts();

        $response = $this
            ->actingAs($this->member)
            ->get(route('posts.index'));

        $response->assertStatus(200);
        $response->assertDontSee('DRAFTED');
    }

    /** @test */
    public function authors_can_view_drafted_and_future_posts_home()
    {
        $this->withoutExceptionHandling();

        $this->setupPosts();

        $response = $this
            ->actingAs($this->author)
            ->get(route('home'));

        $response = $this->get(route('home'));

        $response->assertStatus(200);
        $response->assertSee('DRAFTED');
        $response->assertSee('FUTURE');
    }

    /** @test */
    public function authors_can_edit_own_posts()
    {
        $this->withoutExceptionHandling();

        $this->setupPosts();

        $response = $this
            ->actingAs($this->author)
            ->get(route('posts.edit', $this->author->posts()->first()));

        $response->assertStatus(200);
        $response->assertSee('Drafted');
    }

    /** @test */
    public function authors_can_delete_own_posts()
    {
        $this->withoutExceptionHandling();

        $this->setupPosts();

        $response = $this
            ->actingAs($this->author)
            ->delete(route('posts.destroy', $this->author->posts()->first()));

        $response->assertStatus(302);
        $response->assertDontSee('DRAFTED');
    }

    /** @test */
    public function authors_cannot_delete_member_posts()
    {
        $this->withoutExceptionHandling();

        $this->setupPosts();

        $this->expectException(\Illuminate\Auth\Access\AuthorizationException::class);

        $response = $this
            ->actingAs($this->author)
            ->delete(route('posts.destroy', 1));

        $response->assertStatus(403);
    }
}
