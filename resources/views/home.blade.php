@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @auth
                    <div class="row">
                        <div class="col-6"><h3>Your Posts</h3></div>
                        @if($posts->isNotEmpty())
                            <div class="col-6">
                                <a href="{{ route('posts.create') }}" class="btn btn-primary btn-sm float-right">Add post</a>
                            </div>
                        @endif
                    </div>
                    @forelse ($posts as $post)
                        @component('posts.item', ['post' => $post])
                            <div>
                                @if($post->is_draft)
                                <span class="badge badge-secondary">Draft</span>
                                @endif
                                @if($post->posted_at <= now())
                                    <span class="badge badge-primary">Published</span>
                                @endif
                                @if($post->is_members_only)
                                    <span class="badge badge-success">Members Only</span>
                                @endif
                            </div>
                            <span>Posted: {{ $post->posted_date }}</span> / <span>Updated: {{ $post->updated_date }}</span>
                            <div class="mt-3">
                                <a href="{{ route('posts.show', $post) }}">Detail</a> /
                                <a href="{{ route('posts.edit', $post) }}">Edit</a> /
                                <form action="{{ route('posts.destroy', $post) }}" method="POST" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-link p-0 border-0 text-danger">Delete</button>
                                </form>
                            </div>
                        @endcomponent
                    @empty
                        <p>You have no posts yet. Create a new post <a href="{{ route('posts.create') }}">here</a>.</p>
                    @endforelse
                @endauth

                @guest
                    <p>Please login <a href="{{ route('login') }}">here</a>.</p>
                    <p>If you don't have an account, register <a href="{{ route('register') }}">here</a>.</p>
                @endguest
            </div>
        </div>
    </div>
@endsection
