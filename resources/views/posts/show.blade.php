@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h3>Post Detail</h3>
                @component('posts.item', ['post' => $post])
                    <div class="d-flex justify-content-between">
                        <span>{{ $post->user->name }}</span>
                        <span>{{ $post->posted_date }}</span>
                    </div>
                    <hr>
                    <div>
                        {!! $post->content_with_break !!}
                    </div>
                @endcomponent
            </div>
        </div>
    </div>
@endsection
