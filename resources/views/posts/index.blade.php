@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="h3">All Posts</h1>
                @forelse ($posts as $post)
                    @component('posts.item', ['post' => $post])
                        <div class="d-flex justify-content-between">
                            <span>{{ $post->user->name }}</span>
                            <span>{{ $post->posted_date }}</span>
                        </div>
                    @endcomponent
                @empty
                    <div class="card-body">
                        No post at the moment
                    </div>
                @endforelse
                <div class="mt-5">
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
