<div class="card mt-3">
    <div class="card-header">
        <a href="{{ route('posts.show', $post) }}">{{ $post->uppercase_title }}</a>
    </div>
    <div class="card-body">
        {{ $slot }}
    </div>
</div>